var fs = require('fs'),
		xml2js = require('xml2js');

var parser = new xml2js.Parser();
fs.readFile(__dirname + '/Counties.kml', function(err, data) {
	parser.parseString(data, function (err, result) {
		var theDate = "2000-12-31"; // date of census data is my guess
		var obj = result.Document.Folder[0].Placemark
		var cnt = obj.length;
		var howMany = 0;
		for (var i=0;i<cnt;i++) {
			if (theDate == obj[i].TimeSpan[0].end[0]) {
				var name = obj[i].name[0].split(" (")[0];
				var poly = obj[i].MultiGeometry[0].Polygon[0].outerBoundaryIs[0].LinearRing[0].coordinates[0];
				var points = parseLatLng(poly);
				howMany++;
				writeData(name, points);
			}
	;	}
		console.log(howMany);
	});
});

function writeData(county, data) {
	// file name is county name but needs whitespace and periods removed
	var fileName = county.replace(/[\s\.]+/gi, "").toLowerCase();
	data = "var " + fileName + " = [" + data + "];";
	fs.writeFile(__dirname + '/tmp/' + fileName + '.js', data, function(err){
		if (err) {
			console.log("Error: " + err);
		}
	});
}

function parseLatLng(latlng) {
	var data = latlng.replace(/\s+/g, "").split(",0");
	data.pop();
	return data;
}